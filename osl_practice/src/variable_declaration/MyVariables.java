package variable_declaration;

public class MyVariables {

	int a = 10; // Instance or Non-static variables across the class
	static int b = 100;

	public MyVariables() {

	}

	public static void main(String[] args) {

		MyVariables mv = new MyVariables(); // To access Non static Variables inside a Static Method
		System.out.println(mv.a);
		mv.a++;
		System.out.println(mv.a);

		System.out.println(b);
		b++;
		System.out.println(b);
		System.out.println(Integer.MAX_VALUE);
		System.out.println(Short.MAX_VALUE);
		
	}

}
