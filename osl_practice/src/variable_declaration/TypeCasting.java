package variable_declaration;

public class TypeCasting {

	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int i =100;
		long l = i;//implicit typecasting
		double d = i;
		System.out.println(i);
		System.out.println(l);
		System.out.println(d);
		
		//wrapeer class
		Integer in = new Integer(10);
		int in1= in.intValue();
		int in2 = in;
		System.out.println("Integer" +in);
		System.out.println(in1);
		System.out.println(in2);
		
		
		double d1 = 109.567;
		int i1 = (int) d1;
		short s1 = (short) d1;//Explicit Typecasting
		long l1 = (long) d1;
		byte b1 = (byte) d1;
		System.out.println(i1);
		System.out.println(s1);
		System.out.println(l1);
		System.out.println("Byte Value " +b1);
		

	}

}
