package readwritetojavabjects;

import java.util.Scanner;

public class UserMain {

	public static void main(String[] args) {
		
		User u = new User();
	
		Scanner s = new Scanner(System.in);
		System.out.println("Please enter your ID");
		u.setId(s.nextInt());
		System.out.println("Please enter your name");
		u.setName(s.next());
		System.out.println("Please enter your age");
		u.setAge(s.nextInt());
		System.out.println("Printing Employee Details");
		
		System.out.println(u.getAge());
		System.out.println(u.getId());
		System.out.println(u.getName());

	}

}
