package inner_class;

class MyOuterClass {
	
	private int i=9;
	
	private class MyInnerClass{
		private void disp() {
			System.out.println(i);

		}
		
	}
	
	public static void main(String[] args) {
		MyOuterClass moc = new MyOuterClass();
		MyOuterClass.MyInnerClass mic = moc.new MyInnerClass();
		//MyInnerClass mic = new MyInnerClass();
		mic.disp();
	}

}
