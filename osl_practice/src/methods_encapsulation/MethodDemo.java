package methods_encapsulation;

public class MethodDemo {
	
	private int weight;
	
	public int getWeight() {
		return weight;
	}

	public void setWeight(int newWeight) {
		if(newWeight>0) {
			weight = newWeight;
		}
	}
}
