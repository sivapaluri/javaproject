package methods_encapsulation;

public class Student {

	int rollnumber;
	String name;
    static String college="BVC";
    
    static void change(){
    	college="BVCIT";
    }

	public Student(int rollnumber, String name) {
		super();
		this.rollnumber = rollnumber;
		this.name = name;
	}
    
    void display() {
    	System.out.println(rollnumber+" "+name+" "+college);
    }
    public static void main(String[] args) {
		Student.change();
		Student s1= new Student(101, "Siva");
		Student s2= new Student(102, "Shankar");
		s1.display();
		s1.change();
		s2.display();
	}
	
}
