package javaoperators;

public class Operators {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		String s1 = new String("hello");
	String s2 = new String("hello"); // s1 and s2 are at two different locations
		
//		String s1 = "hello";
//		String s2= "hello"; // same memory location
		
//		if(s1.equalsIgnoreCase(s2)) {
//			System.out.println("Two strings are same"); //validates value
//		}
		if(s1==s2) {
			System.out.println("two strings are same"); //validates address
		}
		else {
			System.out.println("Two strings are not same");
		}

	}

}
