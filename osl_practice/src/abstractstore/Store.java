package abstractstore;

public abstract class Store {
	
	 abstract void refrigirator();
	 abstract void television();

}
