package loopconstructs;

public class BreakStatementDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int [] numbers = {5,10,15,20,25,30};
		String [] names = {"siva","Paluri"};
		
		for (String string : names) {
			if(string=="Paluri") {
				break;
			}
			System.out.println(string);
		}
		
		for (int i : numbers) {
			if(i==20) {
				continue;// condition matches , next statments won't get executed. Points to start of loop
			}
			System.out.println(i);
		}

	}

}
