package array;

import java.util.Scanner;

public class DeleteElement {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int n, x, flag = 0, loc = 0;
		Scanner s = new Scanner(System.in);
		System.out.println("Please select your array size");
		n = s.nextInt();
		int a[] = new int[n];
		System.out.println("Enter all your elements");
		for (int i = 0; i < a.length; i++) {
			a[i] = s.nextInt();

		}
		for (int i = 0; i <n; i++) {
			System.out.println("Values entered by you " + a[i]);
		}
		System.out.println("now enter the value that needs to be deleted");
		x = s.nextInt();
		for (int i = 0; i < a.length; i++) {
			if (a[i] == x) {
				flag = 1;
				loc = i;
				break;
			} else {
				flag = 0;
			}
		}

		if (flag == 1) {
			for (int i = loc + 1; i < a.length; i++) {
				a[i-1] = a[i];
			}
			System.out.println("After deleting one element");
			for (int i = 0; i < n-1; i++) {
				System.out.println(a[i]);
			}
		}
	}

}
