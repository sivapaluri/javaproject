package exception_learn;

public class Validation {
	
	boolean isvalidempid(String empid) throws InvalidIdException{
		if(empid.length()>7) {
			throw new InvalidIdException("empID should be 7 digits");
		}
		return true;
		
	}

}
