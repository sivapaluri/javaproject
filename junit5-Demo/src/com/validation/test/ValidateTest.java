package com.validation.test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.validation.Validate;

class ValidateTest {
	
	private static Validate validate;
	@BeforeAll
	
	public static void setUpValiate() {
		validate = new Validate();
	}
	

	@Test
	void testIsPrime() {
		Assertions.assertTrue(validate.isPrime(-9));
	}
	
	@Test
	void testForNegativeIsPrime() {
		try {
			validate.isPrime(-9);
			fail("It didn't work");
		}
		catch(ArithmeticException e){
			Assertions.assertEquals("0 and -ve values are not allowed", e.getMessage());
		}
	}

}
