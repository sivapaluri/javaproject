package com.validation;

public class Validate {
	
	public boolean isPrime(int number) {
		boolean b = false;
		
		int c=0;
		
		if(number <= 0) {
			throw new ArithmeticException("0 and -ve values are not allowed");
		}
		
		for (int i = 1; i <= number ; i++) {
			
			if(number%i==0) {
				c++;
			}
			
		}
		if(c==2) {
			b=true;
		}
		return b;
	}

}
