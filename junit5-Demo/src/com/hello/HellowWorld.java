package com.hello;

public class HellowWorld {
	
	public String sayHello() {
		return "Hello World";
	}

	public String sayHello(String name) {
		return "Hello "+name;
	}
}
