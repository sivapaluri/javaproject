package com.hello.test;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.hello.HellowWorld;

public class HelloWorldTest {

	private static HellowWorld hello;

	@BeforeAll
	public static void setup() {
		hello = new HellowWorld();
	}

	// return type of all testcases would be void

	@Test
	public void testSayHello() {
		//String result = hello.sayHello(); // message from my class
		
		//can pass result to assertion or directly hello.sayHello()

		// Assertions will come into picutre if we have to ask Junit to compare actual &
		// expected
		
		Assertions.assertEquals("Hello World", hello.sayHello());
	}
	
	@Test
	public void testSayHelloWithName() {
		
		String name = "Siva";
		Assertions.assertEquals("Hello "+name, hello.sayHello(name));
	}

}
