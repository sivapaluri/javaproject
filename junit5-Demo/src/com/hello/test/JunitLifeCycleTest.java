package com.hello.test;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class JunitLifeCycleTest {

	@BeforeAll

	// junit4 - > @BeforeClass -- > Onetime
	// this method executes before any method getting executed in this class

	public static void beforeAllMethods() {
		System.out.println("This is first method to be executed");
	}

	@BeforeEach

	// junit 4 -- @Before
	// before everysingle testcase , this method will be executed
	public void beforeEachTestMethod() {
		System.out.println("Before Each test ,this method to be executed");
	}

	@Test

	public void testHi() {
		System.out.println("Hi from Test case");
	}

	@Test
	public void testHello() {
		System.out.println("Hello from Test case");
	}

	@AfterEach

	// junit4 --> @After
	public void afterEachTestMethod() {
		System.out.println("After Each test ,this method to be executed");
	}

	@AfterAll
	// Junit4 --> @AfterClass

	public static void afterAllMethodsInClass() {
		System.out.println("AfterAll methods inthe class");
	}

}
