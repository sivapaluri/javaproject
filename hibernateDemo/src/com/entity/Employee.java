package com.entity;


import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "dell_emp")
public class Employee {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY) // to generate Auto key for ID and it will over ID Setemp ID in this case
	@Column(name = "emp_id")
	private int empID;
	@OneToMany(cascade = CascadeType.ALL) // one employee have many address
	
	private List<Address> address;
	
	public List<Address> getAddress() {
		return address;
	}
	public void setAddress(List<Address> address) {
		this.address = address;
	}
	private String empName;
	public int getEmpID() {
		return empID;
	}
	public void setEmpID(int empID) {
		this.empID = empID;
	}
	public String getEmpName() {
		return empName;
	}
	public void setEmpName(String empName) {
		this.empName = empName;
	}
	
	
	

}
