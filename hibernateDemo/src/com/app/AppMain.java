package com.app;

import java.util.ArrayList;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.entity.Address;
import com.entity.Employee;
import com.utility.HibernateUtility;

public class AppMain {

	public static void main(String[] args) {

		Session session = HibernateUtility.getSession();

		Transaction tx = session.beginTransaction();

		/*
		 * Employee emp = (Employee)session.get(Employee.class, 1);
		 * 
		 * System.out.println(emp.getEmpName());
		 */

		/*
		 * Employee emp = new Employee();
		 * 
		 * emp.setEmpID(101); emp.setEmpName("Admin"); emp.setEmpAddress("Bangalore");
		 * 
		 * session.save(emp);// Stores record in first level cache session.clear();
		 * tx.commit(); System.out.println("Employee record saved");
		 */

		ArrayList<Address> address = new ArrayList<Address>();

		Address adr1 = new Address();
		adr1.setFullAddress("Pune");

		Address adr2 = new Address();
		adr2.setFullAddress("Bangalore");

		address.add(adr1);
		address.add(adr2);
		Employee emp = new Employee();
		emp.setEmpName("Siva");
		emp.setAddress(address);
		session.save(emp);
		tx.commit();
	}

}
