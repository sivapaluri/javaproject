package com.utility;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;

public class HibernateUtility {

	private static SessionFactory sf;

	// Whenever a function is called this static block will be executed firstly
	// Configuring a session factory and obtaining open session using getSession
	// method

	static {
		sf = new AnnotationConfiguration().configure().buildSessionFactory();
	}

	public static Session getSession() {
		Session session = sf.openSession();
		return session;
	}

}
