package custom_exception;

public class Validation {
	
	boolean isvalidpassword(String password) throws InvalidPasswordException{
		
		if(password.length()<5) {
			throw new InvalidPasswordException("Password should be min of 8 characters"); //to throw manual exception
		}
		
		if(password.length()>12) {
			throw new InvalidPasswordException("Password should be max of 12 characters");
		}
		
		return true;
	}
		
		boolean isvalidpincode(String pincode) throws InvalidPincodeException{
			
			if(pincode.length()>6 && pincode.length()<6) {
				throw new InvalidPincodeException("Pincode should be 6 characters");
			}
		
		return true;
}
		
	}



