package Pojo;

public class Employee extends Person {
	private String companyName;
	private int empId;

	public Employee() {
		// TODO Auto-generated constructor stub
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public int getEmpId() {
		return empId;
	}

	public void setEmpId(int empId) {
		this.empId = empId;
	}

	public Employee(String companyName, int empId, String name, int id) {
		super(name, id);
		this.companyName = companyName;
		this.empId = empId;
	}

	public void printEmployee() {
		System.out.println("Name of company is " + companyName);
		System.out.println("Employee Id is " + empId);
		super.printPerson();
	}

}
