package threadingconcept;

public class ThreadJoin {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Thread th1 = new Thread(new MyClass2(), "th1");
		Thread th2 = new Thread(new MyClass2(), "th2");
		Thread th3 = new Thread(new MyClass2(), "th3");

		// start first thread immediately

		th1.start();

		/* start second thread (th2) once the first thread is dead */
		try {
			th1.join(); //th2 will not be given control unless & until th1 is completed 
			//th1.yield();
		} catch (InterruptedException ie) {
			ie.printStackTrace();
		}
		th2.start();
		/* start third thread (th2) once the second thread is dead */
		try {
			th2.join();
		} catch (InterruptedException ie) {
			ie.printStackTrace();
		}
		th3.start();
		// displaying a message once third thread is dead
		try {
			th3.join();
		} catch (InterruptedException ie) {
			ie.printStackTrace();
		}
		System.out.println("All threads have finished execution");

	}
}
