package com.test.service;

import java.util.List;

import com.test.model.User;

public interface UserService {
	
	public User createUser(User user);
	public List<User> getUsers(User user);

}
