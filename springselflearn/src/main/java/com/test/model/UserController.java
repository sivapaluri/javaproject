package com.test.model;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.test.serviceImpl.UserServiceImpl;


@RestController
@CrossOrigin
@RequestMapping("/")
public class UserController {
	
	@Autowired
	UserServiceImpl service;
	
	@PostMapping("/user")
	public User createUser(@RequestBody User user) {
		return service.createUser(user);
	}
	
	@GetMapping("/users")
	public List<User> getUsers(User user) {
		return service.getUsers(user);
	}
	
	

}
