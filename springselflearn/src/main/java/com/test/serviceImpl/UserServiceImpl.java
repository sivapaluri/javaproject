package com.test.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.test.model.User;
import com.test.repository.UserRepository;
import com.test.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	UserRepository userrepository;
	
	@Override
	public User createUser(User user) {
		return userrepository.save(user);
	}

	@Override
	public List<User> getUsers(User user) {
		
		return userrepository.findAll();
	}

}
