package com.mvc.controller;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HelloController {
	
	@RequestMapping("/")
	public String showWelcome() {
		return "welcome";
	}
    @RequestMapping("/hello")
    public String sayHi( HttpServletRequest request, ModelMap map) {
    	map.addAttribute("name", request.getParameter("name"));
    	return "hello";
    }
}
