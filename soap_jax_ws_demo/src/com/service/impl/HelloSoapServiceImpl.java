package com.service.impl;

import javax.jws.WebService;

import com.service.HelloSoapService;


@WebService(endpointInterface = "com.service.HelloSoapService")
public class HelloSoapServiceImpl implements HelloSoapService {

	@Override
	public String sayHello() {
		
		return "Hello SOAP with JAX-WS RPC Style";
	}

	@Override
	public String sayHello(String name) {
		
		return "Hello "+name+"welcome to SOAP with JAX-WS RPC style";
	}

}
