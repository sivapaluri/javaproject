package com.service.client;

import java.net.URL;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;





public class HelloSoapClient {
	
	public static void main(String[] args) {
		
		try {
			URL url = new  URL("http://localhost:8000/hello?wsdl");
			QName qName = new QName("http://service.com/","HelloSoapServiceImplService");
		Service service = service.create(url, qName);
		}
		catch(MalformedURIException e) {
			System.out.println(e.getMessage());
		}
		
	}

}
