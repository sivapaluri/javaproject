package com.service;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

@WebService // to make this as webservice
@SOAPBinding(style = Style.RPC)
public interface HelloSoapService {
	
	@WebMethod
	public String sayHello();
	
	@WebMethod
	public String sayHello(String name);
	
	

}
