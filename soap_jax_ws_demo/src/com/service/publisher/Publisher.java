package com.service.publisher;

import javax.xml.ws.Endpoint;

import com.service.impl.HelloSoapServiceImpl;

public class Publisher {

	public static void main(String[] args) {
		String url="http://localhost:8000/hello";
		Endpoint.publish(url, new HelloSoapServiceImpl());
		System.out.println("Service is running on url" +url);
		System.out.println("open your fvt browser and append ?wsdl at the end to see the contract");

	}

}
