package collections;

import java.util.*;

public class SetExample {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Set collection = new HashSet();

		insertElements(collection);

		// System.out.println(collection); not supposed to print collection
		printElements(collection);
		// findElement(collection);

		// collection.size();

	}

	private static void printElements(Set collection) {
		// TODO Auto-generated method stub
//		Object[] elements = collection.toArray(); // not supposed  to print collection- Two objects in heap memory - Collection object and Object[]
//		for (int i = 0; i < elements.length; i++) {
//			System.out.println(elements[i]);
//		}

//		Iterator iterator = collection.iterator();
//		while (iterator.hasNext()) {
//			System.out.println(iterator.next());  //--Best way before java 5
//		}

//		for (Object elements : collection) {
//			System.out.println(elements);            // for each loop , till java7
//		}

		// lambda expression java 8
		collection.forEach(element -> System.out.println(element));

	}

	private static void insertElements(Set collection) {
		collection.add("One");
		collection.add("Second");
		collection.add("third");

		collection.add(new Integer(4));
		collection.add(new Float(3.14F)); // boxing

		collection.add("Second"); // duplicate is not permitted.
		collection.add(new Integer(4)); // duplicate is not permitted in set

	}

	private static void findElement(Set collection) {
		if (collection.contains("One")) {
			System.out.println("Element found");
		} else {
			System.out.println("Element is not available");
		}

	}

}
