package com.log4jdemo;

import org.apache.log4j.Logger;

public class LoggerMain {

	private static Logger log = Logger.getLogger(LoggerMain.class);
	public static void main(String[] args) {
log.trace("Hello Trace");
log.debug("Hello Debug");
log.info("Hello info");
log.warn("Hello warn");
log.error("Hello error");
log.fatal("Hello FATAL");

	}

}
