package example;

import java.io.Serializable;

public class Employee implements Serializable{
  private String name;
  private int age;
  private char gender;
  private long contact;
  private double salary;
  
  public Employee() {
	// TODO Auto-generated constructor stub
}

public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}

public int getAge() {
	return age;
}

public void setAge(int age) {
	this.age = age;
}

public char getGender() {
	return gender;
}

public void setGender(char gender) {
	this.gender = gender;
}

public long getContact() {
	return contact;
}

public void setContact(long contact) {
	this.contact = contact;
}

public double getSalary() {
	return salary;
}

public void setSalary(double salary) {
	this.salary = salary;
}

public Employee(String name, int age, char gender, long contact, double salary) {
	super();
	this.name = name;
	this.age = age;
	this.gender = gender;
	this.contact = contact;
	this.salary = salary;
}

@Override
public String toString() {
	return "Employee [name=" + name + ", age=" + age + ", gender=" + gender + ", contact=" + contact + ", salary="
			+ salary + "]";
}


  
}
