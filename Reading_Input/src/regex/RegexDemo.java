package regex;

public class RegexDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		/*
		 * Regular expression(RegEx) - Pattern matching algorithm.
		 * Applicable to all modular programming languages.
		 * 
		 * [] -represents expression
		 * {} -represents length
		 * [a-z]{2} - any two lower case letters
		 * [A-Z]{6} - any 6 upper case letters
		 * [a-zA-Z]{5} - any alphabet of length 5
		 * [0-9]{4,6} - any digit of length min of 4 and max to 6
		 * [0-9]{1,} - min 1 digti and no limit on max
		 * ^ - represents not
		 * [^0-9] - apart from digits
		 */
		
		String pan = "ABCDE1234A";
		if(pan.matches("[A-Z]{5}[0-9]{4}[A-Z]{1}")) {
			System.out.println("Valid PAN");
		}else
		{
			System.out.println("Invalid Pan");
		}
		

//		String s="Helloo dd #33455   ii b @";
//		System.out.println("Total chars "+s.length());
//		System.out.println(s.replaceAll("[^a-zA-Z]",""));
//		System.out.println(s.replaceAll("[^a-zA-Z]","").length());//replacing all otherthan alphabets
//		System.out.println(s.replaceAll("[a-zA-Z]",""));
//		System.out.println(s.replaceAll("[a-zA-Z]","").length());//replacing alphabets
//		System.out.println(s.replaceAll("[^a-zA-Z0-9]",""));
//		System.out.println(s.replaceAll("[^a-zA-Z0-9]","").length());//replacing all other than alphabets,numbers
//		System.out.println(s.replaceAll("[^ ]", "").length());// replace all except spaces
//		
//		System.out.println(s.replaceAll("[ ]{2,}", ""));
	}
}
