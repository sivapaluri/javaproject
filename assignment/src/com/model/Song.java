package com.model;

import java.io.Serializable;

public class Song implements Serializable{

private String singerName;
private String albumName;
private String rating;
private String price;

public Song() {
	// TODO Auto-generated constructor stub
}

public String getSingerName() {
	return singerName;
}

public void setSingerName(String singerName) {
	this.singerName = singerName;
}

public String getAlbumName() {
	return albumName;
}

public void setAlbumName(String albumName) {
	this.albumName = albumName;
}

public String getRating() {
	return rating;
}

public void setRating(String rating) {
	this.rating = rating;
}

public String getPrice() {
	return price;
}

public void setPrice(String price) {
	this.price = price;
}

public Song(String singerName, String albumName, String rating, String price) {
	super();
	this.singerName = singerName;
	this.albumName = albumName;
	this.rating = rating;
	this.price = price;
}

@Override
public String toString() {
	return "Song [singerName=" + singerName + ", albumName=" + albumName + ", rating=" + rating + ", price=" + price
			+ "]";
}




}
