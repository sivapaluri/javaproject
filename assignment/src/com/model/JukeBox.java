package com.model;

import java.io.*;
import java.util.*;

public class JukeBox {

	ArrayList<Song> songList = new ArrayList<Song>();

	public static void main(String[] args) {
		new JukeBox().go();

	}
	
	public void go() {
		getSongS();
		System.out.println(songList);
	}
	
	void getSongS() {
		
		
		try {
			File file = new File("song.txt");
			BufferedReader br = new BufferedReader(new FileReader(file));
			String line=null;
			while((line=br.readLine())!=null) {
				addSong(line);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}
	void addSong(String lineToParse) {
		String[] tokens = lineToParse.split("/");
		
		Song nextSong = new Song(tokens[0],tokens[1],tokens[2],tokens[3]);
		songList.add(nextSong);
	}

}
