package hacker_rank;

import java.util.Scanner;

public class Solution2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner s = new Scanner(System.in);
		int n = s.nextInt();
		double d = s.nextDouble();
		s.nextLine(); // If you use the nextLine() method immediately following the nextInt() method,
						// recall that nextInt() reads integer tokens; because of this, the last newline
						// character for that line of integer input is still queued in the input buffer
						// and the next nextLine() will be reading the remainder of the integer line
						// (which is empty).
		String sentence = s.nextLine();
		System.out.println(n);
		System.out.println(d);
		System.out.println(sentence);
	}

}
