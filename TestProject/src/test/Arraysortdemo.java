package test;

import java.util.Arrays;

public class Arraysortdemo {
	
	public static void main(String[] args) {
		
		 int[] arr = {5,3,0,10,19,100};
		 Arrays.sort(arr);

	        System.out.printf("Modified arr[] : %s", 
	                          Arrays.toString(arr)); 
		 
		 
	}

}
