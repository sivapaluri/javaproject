package test;

import java.util.*;

public class NestedList {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		List list = new ArrayList(); // List is an interface and we cannot create object.
		list.add("Siva");
		list.add("Dell");

		ArrayList al = new ArrayList();
		al.add(list);
		al.add(1, "Sreeni");
		al.add(1, "Vijay");
		System.out.println(al);

	}

}
