package abstraction;

public abstract class LMS {

	public abstract void accessLms();

	public abstract void navigateToCourse();

	public abstract void takeAssessment();

}
