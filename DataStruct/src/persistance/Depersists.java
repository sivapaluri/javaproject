package persistance;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;

public class Depersists {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		try {
			ObjectInputStream in = new ObjectInputStream(new FileInputStream("file.txt"));
			Student s = (Student) in.readObject();
			//printing the data of serialized object
			System.out.println(s.id + " "+s.name);
			//closing the stream
			in.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}

}
