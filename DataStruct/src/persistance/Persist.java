package persistance;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class Persist {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Student s1 = new Student(100, "Siva Paluri");
		
		//creating stream and writing the object
		
		try {
			FileOutputStream fout = new FileOutputStream("file.txt");
			ObjectOutputStream objout = new ObjectOutputStream(fout);
			
			objout.writeObject(s1);
			objout.flush();
			System.out.println("success");
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
