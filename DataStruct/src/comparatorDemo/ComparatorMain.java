package comparatorDemo;

import java.util.ArrayList;
import java.util.Collections;

public class ComparatorMain {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		ArrayList<Student> ar = new ArrayList<Student>();
		ar.add(new Student(111,"Siva","London"));
		ar.add(new Student(112,"Shankar","NewYork"));
		ar.add(new Student(113,"Sai","Paris"));
		
		System.out.println("unsorted");
		
		for (int i = 0; i < ar.size(); i++) {
			System.out.println(ar.get(i));
			
		}
		Collections.sort(ar, new Sortbyroll());
		System.out.println("sorted by roll no");
		
		for (int i = 0; i < ar.size(); i++) {
			System.out.println(ar.get(i));
			
		}
		Collections.sort(ar, new Sortbyname());
		System.out.println("sorted by name");
		for (int i = 0; i < ar.size(); i++) {
			System.out.println(ar.get(i));
			
		}

	}

}
