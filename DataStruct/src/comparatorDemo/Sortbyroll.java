package comparatorDemo;

import java.util.Comparator;

public class Sortbyroll implements Comparator<Student> {

	@Override
	public int compare(Student o1, Student o2) {
		// used for sorting in ascending order of roll number
		return o2.rollno - o1.rollno;// descending order
	//	return o1.rollno - o2.rollno; ascending order
		
	}

}
