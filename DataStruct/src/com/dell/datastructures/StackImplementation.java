package com.dell.datastructures;

import java.util.Stack;

public class StackImplementation {
	
	//pushing element on the top of the stack
	
	static void stack_push(Stack<Integer> stack) {
		
		for (int i = 0; i < 5; i++) {
			stack.push(i);
			
		}
		System.out.println("Stack Push :");
		System.out.println(stack);
		
	}
	
	//popping element from top of the stack
	
	static void stack_pop(Stack<Integer> stack) {
		
		System.out.println("Pop :");
		
		for (int i = 0; i < 5; i++) {
			Integer y = stack.pop();
			System.out.println(y);
		}
	}
	
	//display element on top of stack
	//Stack<Integer> is the variable declarationID for stack
	static void stack_peek(Stack<Integer> stack) {
		Integer element = (Integer) stack.peek();
		System.out.println("Element at top of stack :"+ element);
	}
	
	//searching element in stack
	static void stack_search(Stack<Integer> stack , int element) {
		Integer pos = (Integer) stack.search(element);
		
		if(pos==-1)
			System.out.println("Element not found");
		else
			System.out.println("Element is found at position " +pos);
	}
	
	

}
