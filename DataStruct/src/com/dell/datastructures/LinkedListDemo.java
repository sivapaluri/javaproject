package com.dell.datastructures;

import java.util.LinkedList;

//can use Linkedlist class to avoid boiler plate code
public class LinkedListDemo extends LinkedList{
	
	Node head;
	Node tail;
	
	//This function prints contents of the linked list starting from head
	
	public void display() {
		Node n = head;
		while(n!=null) {
			System.out.println(n.data+ "\n");
			n=n.next;
		}
	}
	
	//Method to create a simple linked list with 3 nodes
	public static void main(String[] args) {
		LinkedListDemo list = new LinkedListDemo();
		
		list.head = new Node(100);
		Node second = new Node(200);
		Node third = new Node(300);
		
		list.head.next = second; //link first node with second node
		second.next = third;// link third node with second node
		list.display();
	}
	

}
