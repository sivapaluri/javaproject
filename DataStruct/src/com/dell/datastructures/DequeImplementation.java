package com.dell.datastructures;

import java.util.Deque;
import java.util.Iterator;
import java.util.LinkedList;

public class DequeImplementation {
	
	public static void main(String[] args) {
		
		Deque<String> deque = new LinkedList<String>();
		
		//We can add elements to the queue in various ways
		
		
		deque.add("Element 1(Tail)");
		deque.addFirst("Element 2 (head)");
		deque.addLast("Element 3(Tail)");
		deque.push("Element 4 (head)");//add to head
		deque.offer("Element 5 (Tail)");
		deque.offerFirst("Element 6 (Head)");
	deque.offerLast("Element 7 (Tail)");
		
		System.out.println(deque + "\n");
		
		//Iterate through the queue elements
		
		for (Iterator iterator = deque.iterator(); iterator.hasNext();) {
			String string = (String) iterator.next();
			System.out.println(string);
		}
		
	}

}
