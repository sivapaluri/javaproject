package com.dell.datastructures;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

public class ArrayListDemo {
	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		@SuppressWarnings("rawtypes")
		ArrayList arrayList = new ArrayList();
		arrayList.add(1);
		arrayList.add(new Integer(1));//before JDK 1.5
		arrayList.add("Dell");
		arrayList.add(null);
		arrayList.add(null);
		arrayList.add("Dell");
		int s = arrayList.size();
		
//		Iterator i = arrayList.iterator();
//		while(i.hasNext()) {
//			System.out.println(i.next());
//		}
		
		for (Object object : arrayList) {
			
		}
		System.out.println(arrayList);
		
//System.out.println(s);
		//System.out.println(arrayList);
		
//		arrayList.removeAll(arrayList);
//		int s2 = arrayList.size();
//		System.out.println(s2);
//		
//		if(arrayList.isEmpty()) {
//			System.out.println("array list is empty");
//		}else {
//			//System.out.println(arrayList);
//		}
Collections.synchronizedList(arrayList); //at a time one operation is allowed
	}

}
