package com.dell.datastructures;

class CreateList {
	
	public class Node {
		int data;
		Node next;
		
		public Node(int data) {
			this.data=data;
		}
	}
	
	//Declaring head and tail pointers as null
	
	public Node head = null;
	public Node tail= null;
	
	//This function will add the new node at the end of list
	
	public void add(int data) {
		//create new node
		Node newNode = new Node(data);
		//check if the list is empty
		
		if(head==null) {
			//if list is empty, both head and tail would point to new node
			
			head = newNode;
			tail = newNode;
			newNode.next=head;
		}
		else {
			//tail will point to new node
			
			tail.next=newNode;
			//new node will become new tail
			tail = newNode;
			//since it is circular linked list tail will point to head
			tail.next= head;
		}
	}
	
	//Displays all the nodes in the list
	public void display() {
		Node current = head;
		if(head == null) {
			System.out.println("List is empty");
		}
		else {
			System.out.println("Nodes of the circular linked list :");
			do {
				//prints each node by incrementing pointer
				System.out.println(" "+current.data);
				current = current.next;
			}while(current !=head);
			System.out.println();
		}
	}

}

 
