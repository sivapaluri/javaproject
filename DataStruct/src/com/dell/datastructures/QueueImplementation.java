package com.dell.datastructures;

import java.util.LinkedList;
import java.util.Queue;

public class QueueImplementation {

	public static void main(String[] args) {

		Queue<Integer> q = new LinkedList<>();

		// adding elements to queue

		for (int i = 0; i < 5; i++) {
			q.add(i);

		}

		// display elements of queue
		System.out.println("Elements of queue -" + q);

		// to remove head of the queue
		int removedelement = q.remove();
		System.out.println(removedelement + " is the removed element");
		System.out.println(q);

		// to view the head of the queue
		int head = q.peek();
		System.out.println("Head of the queue " + head);
		
		int size = q.size();
		System.out.println("Size of queue is "+size);
		
		int hashvalue=q.hashCode();
		System.out.println("Hash value of queue is " +hashvalue);

	}

}
