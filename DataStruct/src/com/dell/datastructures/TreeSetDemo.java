package com.dell.datastructures;

import java.util.TreeSet;

public class TreeSetDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		TreeSet<Employee> treeset = new TreeSet<Employee>();

		treeset.add(new Employee(101, "Siva", "GlobalIT"));
		treeset.add(new Employee(102, "Raj", "GBS"));
//		treeset.add(null);
//		treeset.add(null);

		for (Employee employee : treeset) {
			System.out.println(employee);
		}

	}

}
