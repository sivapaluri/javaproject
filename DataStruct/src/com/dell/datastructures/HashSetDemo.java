package com.dell.datastructures;

import java.util.HashSet;

public class HashSetDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		/*
		 * no insertion order, no duplicates will be displayed.but no compilation error
		 * because of duplicates
		 */
		
		//Inserting employee data in to Hashset using generics
		
		HashSet<Employee> hashset = new HashSet<Employee>(); 
		hashset.add(new Employee(101, "Siva", "GlobalIT"));
		hashset.add(new Employee(102, "Raj", "GBS"));
		hashset.add(null);
		hashset.add(null);
		
		
		/*
		 * hashset.add("abc"); hashset.add("Dell"); hashset.add("abc");
		 * 
		 * hashset.add("siva"); hashset.add("Dell");
		 */
		//System.out.println(hashset);
		
		for (Employee employee : hashset) {
			System.out.println(employee);
		}

	}

}
