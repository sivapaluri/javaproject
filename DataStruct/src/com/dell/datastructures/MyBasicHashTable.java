package com.dell.datastructures;

import java.util.Hashtable;

public class MyBasicHashTable {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Hashtable<String, String> hm = new Hashtable<String, String>();
        //add key-value pair to hashmap
        hm.put("first", "FIRST INSERTED");
        hm.put("second", "SECOND INSERTED");
        hm.put("third","THIRD INSERTED");
        hm.put(null,null);
        hm.put(null,null);
        System.out.println(hm);
        System.out.println("Size of the HashMap: "+hm.size());
        //getting value for the given key from hashtable
        System.out.println("Value of second: "+hm.get("second"));
        System.out.println("Is HashMap empty? "+hm.isEmpty());
        //removing third key
        hm.remove("third");
        System.out.println(hm);
        System.out.println("Size of the HashMap: "+hm.size());

	}

}
