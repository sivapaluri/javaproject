package com.dell.datastructures.duplicateKeyexample;


public class Price {
	
	private String item;
	private int price;
	public Price(String item, int price) {
		super();
		this.item = item;
		this.price = price;
	}
	public String getItem() {
		return item;
	}
	public void setItem(String item) {
		this.item = item;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		int hashcode =0;
		hashcode= price*20;
		hashcode += item.hashCode();
		return hashcode;
	}
	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		return super.equals(obj);
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "item: "+item+" price: "+price;
	}
	
	

}
