package moviereview.model;

public class Movie {
	
	private String moviename;
	private int releaseyear;
	private float rating;
	
	public Movie() {
		// TODO Auto-generated constructor stub
	}

	public String getMoviename() {
		return moviename;
	}

	public void setMoviename(String moviename) {
		this.moviename = moviename;
	}

	public int getReleaseyear() {
		return releaseyear;
	}

	public void setReleaseyear(int releaseyear) {
		this.releaseyear = releaseyear;
	}

	public float getRating() {
		return rating;
	}

	public void setRating(float rating) {
		this.rating = rating;
	}

	@Override
	public String toString() {
		return "Movie [moviename=" + moviename + ", releaseyear=" + releaseyear + ", rating=" + rating + "]";
	}

	public Movie(String moviename, int releaseyear, float rating) {
		super();
		this.moviename = moviename;
		this.releaseyear = releaseyear;
		this.rating = rating;
	}

	
	

}
