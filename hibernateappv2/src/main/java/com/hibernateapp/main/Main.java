package com.hibernateapp.main;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import com.hibernateapp.pojo.Animal;

public class Main {

	public static void main(String[] args) {

		Configuration configuration = new Configuration().configure(); // pulling the configruation details by
																		// identifying the .cfg file

		StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder()
				.applySettings(configuration.getProperties()); // getting properties in key value pairs

		SessionFactory factory = configuration.buildSessionFactory(builder.build()); // generating session factory using
																						// builder

		Session session = factory.openSession();

		Transaction transaction = session.beginTransaction();

		// Saving Data

//		Animal a1 = new Animal("Tiger");
//		Animal a2 = new Animal("Lion");
//		Animal a3 = new Animal("Zebra");
//		Animal a4 = new Animal("Girafee");
//
//		session.save(a1);
//		session.save(a2);
//		session.save(a3);
//		session.save(a4);

		// Retrieving Data

		// System.out.println(session.get(Animal.class, 2));

		// *Updating Data
//		Animal a = new Animal();
//		a.setId(2);
//		a.setName("Bear");
//		session.update(a);

		// System.out.println(session.get(Animal.class, 2));

		// *Delete
		Animal a = new Animal();
		a.setId(3);
		a.setName("Zebra"); //Try delete without Name parameter
		session.delete(a);
		
		//Query query = session.createQuery("from com.hibernateapp.pojo.Animal");  For all the rows in table
		
//		Query query = session.createQuery("from com.hibernateapp.pojo.Animal where name=:name"); //using where condition
//		
//		query.setString("name", "Tiger");
//		
//		List<Animal> animalList = query.list();
//		
//		for (Animal a:animalList) {
//			
//			System.out.println(a);
//			
//		}

		transaction.commit();
		session.close();

		factory.close();
	}

}
