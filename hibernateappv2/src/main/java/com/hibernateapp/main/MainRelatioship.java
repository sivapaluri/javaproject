package com.hibernateapp.main;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import com.hibernateapp.pojo.Animal;
import com.hibernateapp.pojo.Owner;

public class MainRelatioship {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Configuration configuration = new Configuration().configure(); // pulling the configruation details by
		// identifying the .cfg file

		StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder()
				.applySettings(configuration.getProperties()); // getting properties in key value pairs

		SessionFactory factory = configuration.buildSessionFactory(builder.build()); // generating session factory using
		// builder

		Session session = factory.openSession();

		Transaction transaction = session.beginTransaction();

		Animal a1 = new Animal("Carona");

		Owner o1 = new Owner();
		o1.setName("China");
		o1.getAnimal().add(a1); // try passing animals to o1 object

		session.save(a1);

		session.save(o1);

		transaction.commit();
		session.close();

//System.out.println(session.get(Owner.class, 2));
		factory.close();

	}

}
