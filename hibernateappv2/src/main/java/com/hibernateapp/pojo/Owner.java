package com.hibernateapp.pojo;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table
public class Owner {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int oId;
	private String name;
	//@OneToOne
	//@OneToMany(fetch = FetchType.LAZY)
	@OneToMany
	//private Animal animal;
	private List<Animal> animal = new ArrayList<>(); // every owner has one animal
	
	public Owner() {
		// TODO Auto-generated constructor stub
	}

	public int getoId() {
		return oId;
	}

	public void setoId(int oId) {
		this.oId = oId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Animal> getAnimal() {
		return animal;
	}

	public void setAnimal(List<Animal> animal) {
		this.animal = animal;
	}

	@Override
	public String toString() {
		return "Owner [oId=" + oId + ", name=" + name + ", animal=" + animal + "]";
	}

	public Owner(String name, List<Animal> animal) {
		super();
		this.name = name;
		this.animal = animal;
	}
	
	

}
