package com.ecommerce.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table
@Getter
@Setter
@NoArgsConstructor

public class Registration {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer registrationId;
	private String  firstName;
	private String 	lastName;
	private int age;
	private String sex;
	private String email;
	private long phone;
	private String username;
	private String password;
	private String role="USER";
	
	
	
	
	

}
