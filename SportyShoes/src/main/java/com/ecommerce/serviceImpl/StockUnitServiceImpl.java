package com.ecommerce.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ecommerce.model.StockUnit;

import com.ecommerce.repository.StockUnitRepository;
import com.ecommerce.service.StockUnitService;

@Service
public class StockUnitServiceImpl implements StockUnitService {

	@Autowired
	StockUnitRepository stockRepo;

	@Override
	public StockUnit createStockUnit(StockUnit stockUnit) {

		stockRepo.save(stockUnit);
		return stockUnit;
	}

	@Override
	public List<StockUnit> getAllStock() {
		return stockRepo.findAll();

	}

	@Override
	public List<StockUnit> getAllStockByCategory(String category) {
		// TODO Auto-generated method stub
		return stockRepo.findByCategory(category);
	}

	@Override
	public List<StockUnit> getAllStockByClearance(int clearance) {
		// TODO Auto-generated method stub
		return stockRepo.findByClearance(clearance);
	}

	@Override
	public StockUnit updateStockUnit(StockUnit stockUnit) {
		// TODO Auto-generated method stub
		return stockRepo.save(stockUnit);
	}

	@Override
	public void deleteStockUnit(Integer stockId) {
		// TODO Auto-generated method stub
		 stockRepo.deleteById(stockId);
	}

	@Override
	public List<StockUnit> getAllStockBySport(String sport) {
		// TODO Auto-generated method stub
		return stockRepo.findBySport(sport);
	}

}