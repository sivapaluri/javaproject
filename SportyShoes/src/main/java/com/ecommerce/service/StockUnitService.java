package com.ecommerce.service;



import java.util.List;

import com.ecommerce.model.StockUnit;

public interface StockUnitService {
	
	public StockUnit createStockUnit(StockUnit stockUnit);
	public List<StockUnit> getAllStock();	
	public List<StockUnit> getAllStockByCategory(String category);
	public List<StockUnit> getAllStockBySport(String sport);
	public List<StockUnit> getAllStockByClearance(int clearance);
	public StockUnit updateStockUnit(StockUnit stockUnit);
	public void deleteStockUnit(Integer stockId);


}
