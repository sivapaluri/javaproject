package com.ecommerce.repository;

import org.springframework.data.jpa.repository.JpaRepository;


import com.ecommerce.model.SalesOrder;


public interface OrderRepository extends JpaRepository<SalesOrder, Integer> {

}
