package com.demo.player.model;

public class Player {
	
	private int id;
	private String name;
	private int contact;
	private int age;
	private String teamName;
	
	public Player() {
		// TODO Auto-generated constructor stub
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getContact() {
		return contact;
	}

	public void setContact(int contact) {
		this.contact = contact;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}



	public Player(String name, int contact, int age, String teamName) {
		super();
		this.name = name;
		this.contact = contact;
		this.age = age;
		this.teamName = teamName;
	}

	@Override
	public String toString() {
		return "Player [id=" + id + ", name=" + name + ", contact=" + contact + ", age=" + age + ", teamName="
				+ teamName + "]";
	}
	
	

}
