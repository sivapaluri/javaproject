package com.demo.player;

import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.demo.player.dao.PlayerDAO;
import com.demo.player.model.Player;

public class PlayerMain {

	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("jdbcbean.xml");
		
		PlayerDAO playerDAO = (PlayerDAO) context.getBean("dao");
//		List<Player> playerList = playerDAO.getAllPlayers();
//		
//		System.out.println("Hi");
//		
//		for (Player p : playerList) {
//			System.out.println(p);
//			
//			
//		}
		
		//playerDAO.addPlayer(new Player("Suresh", 949478480, 22, "India"));
		
		System.out.println(playerDAO.getPlayerById(5));
		

	}

}
