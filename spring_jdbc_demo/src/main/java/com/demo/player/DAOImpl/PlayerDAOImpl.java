package com.demo.player.DAOImpl;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;

import com.demo.player.dao.PlayerDAO;
import com.demo.player.model.Player;

public class PlayerDAOImpl implements PlayerDAO{

	private DataSource dataSource;
	private JdbcTemplate jdbcTemplate;
	
	
	
	public DataSource getDataSource() {
		return dataSource;
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
		this.jdbcTemplate= new JdbcTemplate(this.dataSource);
	}

	@Override
	public Player addPlayer(Player player) {
		// TODO Auto-generated method stub
		String sql = "insert into player(name,teamName,age,contact) values(?,?,?,?)";
		jdbcTemplate.update(sql,player.getName(),player.getTeamName(),player.getAge(),player.getContact());
		return null;
	}

	@Override
	public List<Player> getAllPlayers() {
		String sql = "select id,name,teamName,age,contact from player";
		List<Player> playerList = jdbcTemplate.query(sql,new PlayerMapper());
		return playerList;
	}

	@Override
	public Player getPlayerById(int id) {
		String sql = "select id,name,teamName,age,contact from player where id=?";
		Player player =  jdbcTemplate.queryForObject(sql,new Object[] {id}, new PlayerMapper());
		return player;
	}

	@Override
	public void removePlayerById(int id) {
		// TODO Auto-generated method stub
		
	}

}
