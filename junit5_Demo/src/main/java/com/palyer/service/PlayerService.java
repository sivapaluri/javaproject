package com.palyer.service;

import java.util.List;

import com.player.Player;



public interface PlayerService {
	
	public Player createPlayer(Player player);
	public Player updatePlayer(Player player);
	public Player getPlayerById(int id);
	public void removePlayer(int id);
	public List<Player> getallPlayers();

}
