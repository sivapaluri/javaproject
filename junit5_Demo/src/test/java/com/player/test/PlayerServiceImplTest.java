package com.player.test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.palyer.service.PlayerService;
import com.player.Player;
import com.player.serviceimpl.PlayerServiceImpl;

class PlayerServiceImplTest {
	
	private static PlayerService playerservice;
	
	@BeforeAll
	static void setUp() {
		playerservice = new PlayerServiceImpl();
		//playerservice.createPlayer(new Player("Siva"));
	}

	@Test
	void testCreatePlayer() {
		Player player = new Player();
		player.setName("dinesh");
		player = playerservice.createPlayer(player);
		Assertions.assertEquals(1, player.getId());
	}

	@Test
	void testUpdatePlayer() {
		fail("Not yet implemented");
	}

	@Test
	void testGetPlayerById() {
		fail("Not yet implemented");
	}

	@Test
	void testRemovePlayer() {
		fail("Not yet implemented");
	}

	@Test
	void testGetallPlayers() {
		fail("Not yet implemented");
	}

}
