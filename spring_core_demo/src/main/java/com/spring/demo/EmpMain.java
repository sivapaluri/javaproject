package com.spring.demo;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.demo.model.Employee;

public class EmpMain {
	
	public static void main(String[] args) {
		
		ApplicationContext context = new ClassPathXmlApplicationContext("empbean.xml");
		Employee e1 = (Employee) context.getBean("e1");
		System.out.println(e1);
	}

}
