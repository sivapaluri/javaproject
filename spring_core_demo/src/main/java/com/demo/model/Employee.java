package com.demo.model;

import java.util.List;

public class Employee {
	
	private int empId;
	private String empName;
	private List<Address> addressList;
	
	 public Employee() {
		// TODO Auto-generated constructor stub
	}

	public int getEmpId() {
		return empId;
	}

	public void setEmpId(int empId) {
		this.empId = empId;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public List<Address> getAddressList() {
		return addressList;
	}

	public void setAddressList(List<Address> addressList) {
		this.addressList = addressList;
	}

	public Employee(int empId, String empName, List<Address> addressList) {
		super();
		this.empId = empId;
		this.empName = empName;
		this.addressList = addressList;
	}

	@Override
	public String toString() {
		return "Employee [empId=" + empId + ", empName=" + empName + ", addressList=" + addressList + "]";
	}
	 
	 

}
