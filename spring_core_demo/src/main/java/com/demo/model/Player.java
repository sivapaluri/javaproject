package com.demo.model;

public class Player {
	
	private int id;
	private String name;
	
	private Team team;
	
	public Player() {
		// TODO Auto-generated constructor stub
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Team getTeam() {
		return team;
	}

	public void setTeam(Team team) {
		this.team = team;
	}

	public Player(int id, String name) {
		super();
		this.id = id;
		this.name = name;
	}
	
	

	public Player(int id, String name, Team team) {
		super();
		this.id = id;
		this.name = name;
		this.team = team;
	}

	@Override
	public String toString() {
		return "Player [id=" + id + ", name=" + name + ", team=" + team + "]";
	}


	
	

}
