package com.learneracademy.controller;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.learneracademy.Service.TeacherDetailService;
import com.learneracademy.model.TeacherDetail;
import com.learneracademy.serviceImplementation.TeacherDetailServiceImpl;
import com.sun.org.apache.bcel.internal.generic.RETURN;

@Path("/teacherdetail")
public class TeacherDetailController {
	
	TeacherDetailService teacherdetailservice = new TeacherDetailServiceImpl();
	
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public TeacherDetail createTeacherDetail(TeacherDetail teacherdetail) {
		return teacherdetailservice.createTeacherDetail(teacherdetail);
	}

	
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public TeacherDetail updateTeacherDetail(TeacherDetail teacherdetail) {
		return teacherdetailservice.updateTeacherDetail(teacherdetail);
		
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<TeacherDetail> getTeacherDetails(){
		return teacherdetailservice.getTeacherDetails();
		
	}
	
	@Path("/{teacherID}")
	@GET
	public TeacherDetail getTeacherDetailById(@PathParam("teacherID")int id) {
		return teacherdetailservice.getTeacherDetailById(id);
		
	}
	@Path("/{teacherID}")
	@DELETE
	public void removeTeacherDetail(@PathParam("teacherID")int id) {
		teacherdetailservice.removeTeacherDetail(id);
	}

}
