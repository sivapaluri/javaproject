package com.learneracademy.controller;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.learneracademy.Service.SubjectDetailService;
import com.learneracademy.model.SubjectDetail;
import com.learneracademy.serviceImplementation.SubjectDetailServiceImpl;

@Path("/subjectdetail")
public class SubjectDetailController {
	
	SubjectDetailService subjectdetailservice = new SubjectDetailServiceImpl();
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public SubjectDetail createSubjectDetail(SubjectDetail subjectdetail) {
		return subjectdetailservice.createSubjectDetail(subjectdetail);
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<SubjectDetail> getClassDetails() {
		return subjectdetailservice.getSubjectDetails();

	}
	
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public SubjectDetail updateSubjectDetail(SubjectDetail subjectdetail) {
		return subjectdetailservice.updateSubjectDetail(subjectdetail);
		
	}
	
	@Path("/{sNo}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public SubjectDetail getSubjectDetailById(@PathParam("sNo")int id) {
		return subjectdetailservice.getSubjectDetailById(id);
		
	}
	@Path("/{sNo}")
	@DELETE
	public void removeSubjectDetail(@PathParam("sNo") int id) {
		subjectdetailservice.removeSubjectDetail(id);
	}
}
