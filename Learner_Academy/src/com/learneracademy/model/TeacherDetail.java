package com.learneracademy.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;



@Entity
@Table(name = "Teacher")
public class TeacherDetail {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "TEACHER_ID")
	private int teacherID;

	@Column(name = "TEACHER_NAME")
	private String teacherName;

	@Column(name = "TEACHER_CONTACT")
	private long teacherContact;


	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name="teacherID")
	private Set<SubjectDetail> subjects;

	
	public TeacherDetail() {

	}

	public int getTeacherID() {
		return teacherID;
	}

	public void setTeacherID(int teacherID) {
		this.teacherID = teacherID;
	}

	public String getTeacherName() {
		return teacherName;
	}

	public void setTeacherName(String teacherName) {
		this.teacherName = teacherName;
	}

	public long getTeacherContact() {
		return teacherContact;
	}

	public void setTeacherContact(long teacherContact) {
		this.teacherContact = teacherContact;
	}





	public Set<SubjectDetail> getSubjects() {
		return subjects;
	}

	public void setSubjects(Set<SubjectDetail> subjects) {
		this.subjects = subjects;
	}

	public TeacherDetail(String teacherName, long teacherContact, Set<SubjectDetail> subjects) {
		super();
		this.teacherName = teacherName;
		this.teacherContact = teacherContact;
		this.subjects = subjects;
	}

	

}