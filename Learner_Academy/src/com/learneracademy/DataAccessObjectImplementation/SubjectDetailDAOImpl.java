package com.learneracademy.DataAccessObjectImplementation;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;


import com.learneracademy.DataAccessObject.SubjectDetailDAO;
import com.learneracademy.model.ClassDetail;
import com.learneracademy.model.StudentDetail;
import com.learneracademy.model.SubjectDetail;

public class SubjectDetailDAOImpl implements SubjectDetailDAO {

	Configuration configuration = new Configuration().configure();

	StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder()
			.applySettings(configuration.getProperties());

	SessionFactory factory = configuration.buildSessionFactory(builder.build());

	Session session = factory.openSession();

	@Override
	public SubjectDetail createSubjectDetail(SubjectDetail subjectdetail) {

		Transaction transaction = session.beginTransaction();
		session.save(subjectdetail);
		transaction.commit();
		session.close();
		return subjectdetail;
	}

	@Override
	public List<SubjectDetail> getSubjectDetails() {
		Transaction transaction = session.beginTransaction();
		List<SubjectDetail> subjectdetaillist = session.createQuery("from com.learneracademy.model.SubjectDetail").list();
		return subjectdetaillist;
	}

	@Override
	public SubjectDetail updateSubjectDetail(SubjectDetail subjectdetail) {
		Transaction transaction=session.beginTransaction();
		session.update(subjectdetail);
		transaction.commit();
		session.close();
		return subjectdetail;
	}

	@Override
	public SubjectDetail getSubjectDetailById(int sNo) {
		Transaction transaction=session.beginTransaction();
		SubjectDetail subjectdetail=(SubjectDetail) session.get(SubjectDetail.class,sNo);
		transaction.commit();
		session.close();
		return subjectdetail;
	}

	@Override
	public void removeSubjectDetail(int sNo) {
		Transaction transaction=session.beginTransaction();
		SubjectDetail subjectdetail = new SubjectDetail();
		subjectdetail.setsNo(sNo);;
		session.delete(subjectdetail);
		transaction.commit();
		session.close();

	}

	@Override
	public ClassDetail patchSubjectDetail(SubjectDetail subjectdetail) {
		// TODO Auto-generated method stub
		return null;
	}



}
