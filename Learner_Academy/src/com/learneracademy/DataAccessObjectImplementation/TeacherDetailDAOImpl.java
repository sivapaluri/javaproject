package com.learneracademy.DataAccessObjectImplementation;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;


import com.learneracademy.DataAccessObject.TeacherDetailDAO;
import com.learneracademy.model.ClassDetail;
import com.learneracademy.model.SubjectDetail;
import com.learneracademy.model.TeacherDetail;



public class TeacherDetailDAOImpl implements TeacherDetailDAO {

	Configuration configuration = new Configuration().configure();

	StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder()
			.applySettings(configuration.getProperties());

	SessionFactory factory = configuration.buildSessionFactory(builder.build());

	Session session = factory.openSession();

	@Override
	public TeacherDetail createTeacherDetail(TeacherDetail teacherdetail) {

		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		
//		ClassDetail cls = new ClassDetail();
//		session.save(cls);
//		cls.getClassId();
//		
//		TeacherDetail td = new TeacherDetail();
//		td.setTeacherdetail(cls);
//	
		session.save(teacherdetail);

		transaction.commit();
		session.close();
		return teacherdetail;
	}

	@Override
	public List<TeacherDetail> getTeacherDetails() {
		Transaction transaction = session.beginTransaction();
		List<TeacherDetail> teacherdetaillist = session.createQuery("from com.learneracademy.model.TeacherDetail").list();
		return teacherdetaillist;
	}

	@Override
	public TeacherDetail updateTeacherDetail(TeacherDetail teacherdetail) {
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		session.update(teacherdetail);
		transaction.commit();
		session.close();
		return teacherdetail;
	}

	@Override
	public TeacherDetail getTeacherDetailById(int teacherID) {
		Transaction transaction=session.beginTransaction();
		TeacherDetail teacherdetail=(TeacherDetail) session.get(TeacherDetail.class,teacherID);
		transaction.commit();
		session.close();
		return teacherdetail;
	}

	@Override
	public void removeTeacherDetail(int teacherID) {
		Session session = factory.openSession();
		Transaction transaction=session.beginTransaction();
		TeacherDetail teacherdetail = new TeacherDetail();
		teacherdetail.setTeacherID(teacherID);
		session.delete(teacherdetail);
		transaction.commit();
		session.close();

	}

}
