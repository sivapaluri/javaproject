package com.learneracademy.DataAccessObject;

import java.util.List;

import com.learneracademy.model.ClassDetail;

public interface ClassDetailDAO {

	public ClassDetail createClassDetail(ClassDetail classdetail);

	public List<ClassDetail> getClassDetails();

	public ClassDetail updateClassDetail(ClassDetail classdetail);

	public ClassDetail getClassDetailById(int id);
	
	public ClassDetail patchClassDetail(ClassDetail classdetail);

	public void removeClassDetail(int id);

}
