package com.mvc.rest.serviceImpl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.mvc.rest.model.Message;
import com.mvc.rest.service.MessageService;

@Service
public class MessageServiceImpl implements MessageService {
	
	private static Map<Integer,Message> map = new HashMap<Integer, Message>();

	private static int count;

	@Override
	public Message createMessage(Message message) {
		message.setId(++count);
		map.put(count, message);
		return message;
	}

	@Override
	public Message getMessageById(int id) {
	 
		return map.get(id);
	}

	@Override
	public List<Message> getAllMessages() {
		
		return new ArrayList<Message>(map.values());
	}

	@Override
	public Message updateMessage(Message message) {
		map.put(message.getId(), message);
		return message;
	}

	@Override
	public void removeMessage(int id) {
		
		map.remove(id);
	}
}