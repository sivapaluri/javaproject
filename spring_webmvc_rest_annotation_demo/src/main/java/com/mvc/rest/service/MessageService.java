package com.mvc.rest.service;

import java.util.List;

import com.mvc.rest.model.Message;

public interface MessageService {
	
	public Message createMessage(Message message);
	public Message getMessageById(int id);
	public List<Message> getAllMessages();
	public Message updateMessage(Message message);
	public void removeMessage(int id);

}
