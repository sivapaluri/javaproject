package com.mvc.rest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.mvc.rest.model.Message;
import com.mvc.rest.service.MessageService;


@RestController
public class MessageController {

	@Autowired //Field Injection
	private MessageService service;
	
	@PostMapping("/message")
	public Message createMessage(@RequestBody Message message) {
		// TODO Auto-generated method stub
		return service.createMessage(message);
	}

	@GetMapping("/message/{id}")
	public Message getMessageById(@PathVariable("id")int id) {
		// TODO Auto-generated method stub
		return service.getMessageById(id);
	}

	@GetMapping("/messages")
	public List<Message> getAllMessages() {
		// TODO Auto-generated method stub
		return service.getAllMessages();
	}

	@PutMapping("/message")
	public Message updateMessage(@RequestBody Message message) {
		// TODO Auto-generated method stub
		return service.updateMessage(message);
	}

	@DeleteMapping("/message/{id}")
	public void removeMessage(@PathVariable("id")int id) {
		service.removeMessage(id);
		
	}

}
