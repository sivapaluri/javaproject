package abstraction_using_func_interface;

/* Single Abstract method Interface
 * Only one abstract method but can have default or static Method*/
@FunctionalInterface
public interface FuncInterface {
	
	 void Welcome();
	 default void bye() {
		 System.out.println("See you later");
	 }

}
