package abstraction_using_interface;

public interface Myinterface2 {
	
	void deliverproduct();
	void provideinvoice();
	default void update() {
		System.out.println("Product Delivery status updated");
	}

}
