package abstraction_using_interface;

/*Interface should not have any completed methods can have default method with definition
 * Normal Interface
 */
public interface Myinterface {
	
	void open();
	void close();
	default void enter(){
		System.out.println("Enter the Dragon");
	}

}
