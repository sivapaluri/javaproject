package abstraction_using_interface;

public class Interfaceclass implements Myinterface, Myinterface2 {

	@Override
	public void open() {
		// TODO Auto-generated method stub
		System.out.println("Door Opened");

	}

	@Override
	public void close() {
		// TODO Auto-generated method stub
		System.out.println("Door closed");

	}

	@Override
	public void deliverproduct() {
		// TODO Auto-generated method stub
		System.out.println("Product Handed over to customer");

	}

	@Override
	public void provideinvoice() {
		// TODO Auto-generated method stub
		System.out.println("Invoice copy provided");

	}

}
