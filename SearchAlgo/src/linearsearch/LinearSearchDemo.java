package linearsearch;

import java.util.Scanner;

public class LinearSearchDemo {
	
	public static void main(String[] args) {
		int counter,length,item,array[];
		Scanner input = new Scanner(System.in);
		System.out.println("Enter the number of elements you want to enter");
		length = input.nextInt();
		
		//Creating array to store all elements
		
		array = new int[length];
		System.out.println("Enter " + length + " integers");
		//loop to store each element
		
		for (counter= 0; counter < array.length; counter++) {
			array[counter] = input.nextInt();
			}
		
		//prompting for user to enter value that needs to searched
		System.out.println("Enter the search value");
		int search_value = input.nextInt();
		
		//loop to compare value in array with user input 
		for (counter = 0; counter < array.length; counter++) {
			
			if(array[counter]== search_value) {
				System.out.println(search_value +" is present at the location "+(counter+1) );
				break;
			}
			
		}
		if(counter==length)
			System.out.println(search_value + " doesn't exist in array");
	}

}
