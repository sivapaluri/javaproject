package com.bo;

import com.exception.BusinessException;
import com.model.User;

public interface LoginBo {
	
	public boolean isValidUser(User user) throws BusinessException;

}
