package com.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bo.impl.LoginBoImpl;
import com.exception.BusinessException;
import com.model.User;

/**
 * Servlet implementation class LoginController
 */
@WebServlet("/login")
public class LoginController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		response.setContentType("text/html");
		PrintWriter printout = response.getWriter();
		User user = new User(request.getParameter("username"),request.getParameter("password"));
		LoginBoImpl loginBo = new LoginBoImpl();
		RequestDispatcher rd = null;
		
		try {
			if(loginBo.isValidUser(user)) {
				rd = request.getRequestDispatcher("success");
				rd.forward(request, response);
				
			}
		
		}catch(BusinessException e) {
			rd=request.getRequestDispatcher("index.html");
			request.setAttribute("error",e.getMessage());
			rd.include(request, response);
			printout.print("<center><span style='color:red;'>"+e.getMessage()+"</span></center>");
		}
		
	}

}
