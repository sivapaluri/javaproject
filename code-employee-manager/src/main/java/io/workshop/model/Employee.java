package io.workshop.model;


import java.util.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.annotations.GenericGenerator;

@Entity
@XmlRootElement(name="employee")
public class Employee {
	
	@Id
	@GeneratedValue(generator="system-uuid")
	@GenericGenerator(name="system-uuid",strategy="uuid")
	private String id;
	private String name;
	private String phone;
	private String email;
	
	@Column(insertable= true, updatable=false)
	private Date created;
	private Date modified;
	
	public Employee() {
		this.id=UUID.randomUUID().toString();
		this.name=name;
		this.phone=phone;
		this.email=email;
		this.created = new Date();
		this.modified = new Date();
	}


	
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}



	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}



	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}



	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}



	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}



	/**
	 * @param phone the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}



	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}



	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}



	/**
	 * @return the created
	 */
	public Date getCreated() {
		return created;
	}



	/**
	 * @param created the created to set
	 */
	public void setCreated(Date created) {
		this.created = created;
	}



	/**
	 * @return the modified
	 */
	public Date getModified() {
		return modified;
	}



	/**
	 * @param modified the modified to set
	 */
	public void setModified(Date modified) {
		this.modified = modified;
	}



	@PrePersist
void onCreate() {
	this.setCreated(new Date());
	this.setModified(new Date());
}

	@PreUpdate
void onUpdate() {
	
	this.setCreated(new Date());
	this.setModified(new Date());
}
}
