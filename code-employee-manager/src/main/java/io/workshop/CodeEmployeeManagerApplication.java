package io.workshop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CodeEmployeeManagerApplication {

	public static void main(String[] args) {
		SpringApplication.run(CodeEmployeeManagerApplication.class, args);
	}

}
