package io.workshop.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import io.workshop.model.Employee;
import io.workshop.repository.EmployeeRepository;

@RestController
public class EmployeeResource {
	
	@Autowired
	EmployeeRepository employeeRepository;
	
	@GetMapping("/employees")
	public List<Employee> findAllEmployees(){
		return employeeRepository.findAll();
	}
	
	
	//	@GetMapping("/employees/{id}")
	//	public Optional<Employee> employee(@PathVariable("id") String id ) {
	//		return employeeRepository.findById(id);
	//	}
}
