package io.workshop.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;

import io.workshop.model.Employee;

@Transactional
public interface EmployeeRepository extends JpaRepository<Employee , Integer> {

}
