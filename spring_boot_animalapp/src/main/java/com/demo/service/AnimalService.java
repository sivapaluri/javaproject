package com.demo.service;

import java.util.List;

import com.demo.model.Animal;


public interface AnimalService {
	
	public Animal createAnimal(Animal animal);
	public Animal getAnimalById(int id);
	public List<Animal> getAllAnimals();
	public Animal updateAnimal(Animal animal);
	public void removeAnimal(int id);
	public List<Animal> getAnimalByCategory(String category);

}
