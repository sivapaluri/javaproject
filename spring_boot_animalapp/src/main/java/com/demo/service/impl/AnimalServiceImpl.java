package com.demo.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.model.Animal;
import com.demo.repository.AnimalDAO;
import com.demo.service.AnimalService;

@Service
public class AnimalServiceImpl implements AnimalService {
	
	@Autowired
	AnimalDAO dao;

	@Override
	public Animal createAnimal(Animal animal) {
		// TODO Auto-generated method stub
		
		return dao.save(animal);
	}

	@Override
	public Animal getAnimalById(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Animal> getAllAnimals() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Animal updateAnimal(Animal animal) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void removeAnimal(int id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Animal> getAnimalByCategory(String category) {
		// TODO Auto-generated method stub
		return null;
	}

}
