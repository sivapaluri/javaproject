package com.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.demo.model.Animal;

import com.demo.service.AnimalService;


@RestController
@CrossOrigin
@RequestMapping("/")
public class AnimalController{
	
	@Autowired
	AnimalService service;

	@PostMapping("/animal")
	public Animal createAnimal(@RequestBody Animal animal) {
		return service.createAnimal(animal);
	}

	
	public Animal getAnimalById(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	
	public List<Animal> getAllAnimals() {
		// TODO Auto-generated method stub
		return null;
	}

	
	public Animal updateAnimal(Animal animal) {
		// TODO Auto-generated method stub
		return null;
	}

	
	public void removeAnimal(int id) {
		// TODO Auto-generated method stub
		
	}

	
	public List<Animal> getAnimalByCategory(String category) {
		// TODO Auto-generated method stub
		return null;
	}
	
	

}
