package com.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootAnimalappApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootAnimalappApplication.class, args);
	}

}
