package com.demo.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table
@Setter
@Getter
@NoArgsConstructor

public class Animal {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO.IDENTITY)
	private int id;
	private String name;
	private int age;
	private String category;
	private String image;

}
