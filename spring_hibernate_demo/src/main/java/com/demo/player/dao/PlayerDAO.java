package com.demo.player.dao;

import java.util.List;

import com.demo.player.model.Player;

public interface PlayerDAO {
	
	public Player addPlayer(Player player);
	public List<Player> getAllPlayers();
	public Player getPlayerById(int id);
	public void removePlayerById(int id);

}
