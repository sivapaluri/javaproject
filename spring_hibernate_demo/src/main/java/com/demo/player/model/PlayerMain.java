package com.demo.player.model;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.demo.player.dao.PlayerDAO;

public class PlayerMain {

	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("hibernatebeans.xml");
		
		PlayerDAO dao = (PlayerDAO) context.getBean("dao");

	}

}
