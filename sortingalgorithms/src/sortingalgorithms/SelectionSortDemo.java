package sortingalgorithms;

public class SelectionSortDemo {

	public static void selectionsort(int[] arr) {
		for (int i = 0; i < arr.length - 1; i++) {

			int index = i;

			for (int j = i + 1; j < arr.length; j++) {
				if (arr[j] < arr[index]) {
					index = j;
				}

			}
			// swapping code using third variable

			int smallerNumber = arr[index];
			arr[index] = arr[i];
			arr[i] = smallerNumber;
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int arr[] = { 21, 60, 32, 01, 41, 34, 5 };
		System.out.println("Selection sort");
		
		//printing values in the unsorted format
		
		for (int i : arr) {
			System.out.println(i+ " ");
		}
		System.out.println();
		
		selectionsort(arr);
		
		//printing values in the unsorted format
		
		System.out.println("selection sort");
		for (int i : arr) {
			System.out.println(i+" ");
		}

	}

}
