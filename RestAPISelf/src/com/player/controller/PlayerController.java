package com.player.controller;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.player.model.Player;
import com.player.model.Team;
import com.player.service.PlayerService;
import com.player.serviceimpl.PlayerServiceimpl;

@Path("/player")
public class PlayerController {

	private PlayerService service = new PlayerServiceimpl();

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Player createPlayer(Player player) {

		return service.createPlayer(player); // Able to create player and displaying back ID in postman
	}

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Player getPlayerById(@PathParam("id") int id) {
		return service.getPlayerById(id);

	}

	@DELETE
	@Path("/{id}")
	public void removePlayer(@PathParam("id") int id) {
		service.removePlayer(id);

	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Player> getallPlayers() {
		return service.getallPlayers(); // Displaying all players

	}

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Player updatePlayer(Player player) {
		return service.updatePlayer(player);

	}
}
