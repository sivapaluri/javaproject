package com.player.controller;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.player.model.Team;
import com.player.service.PlayerService;
import com.player.service.Teamservice;
import com.player.serviceimpl.PlayerServiceimpl;
import com.player.serviceimpl.TeamServiceImpl;

@Path("/team")
public class TeamController {
	
	private Teamservice tservice = new TeamServiceImpl();
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public  Team createTeam(Team team) {
		return tservice.createTeam(team) ;
											//Able to create player and displaying back ID in postman
	}

}
