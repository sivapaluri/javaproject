package com.player.controller;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.player.model.Match;
import com.player.service.MatchService;
import com.player.serviceimpl.MatchServiceImpl;

@Path("/match")
public class MatchController {
	
	MatchService mservice = new MatchServiceImpl();
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Match createMatch(Match match) {
		return mservice.createMatch(match);
		
	}
	

}
