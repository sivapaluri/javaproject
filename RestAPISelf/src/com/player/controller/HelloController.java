package com.player.controller;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.Path;

@Path("/")
public class HelloController {

	@GET
	public String sayhelloGet() {

		return "Welcome to JAX-RS Controller GET";

	}

	@Path("/hipost")
	@POST
	public String sayhelloPost() {

		return "Welcome to JAX-RS Controller POST";

	}

	@Path("/patch")
	@PATCH
	public String sayhelloPatch() {

		return "Welcome to JAX-RS Controller PATCH";

	}

	@DELETE
	public String sayhelloDelete() {

		return "Welcome to JAX-RS Controller DELETE";

	}

}
