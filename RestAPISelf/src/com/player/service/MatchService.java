package com.player.service;

import com.player.model.Match;

public interface MatchService {
	
	public Match createMatch(Match match);

}
