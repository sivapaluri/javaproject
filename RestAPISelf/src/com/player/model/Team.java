package com.player.model;


import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

import javax.persistence.Table;

@Entity
@Table (name = "Team")
public class Team {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int teamId;
	
	private String teamName;
	
	@OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
	@JoinColumn(name="teamId")
	private Set<Player> players = new HashSet<>();
	
public Team() {
	// TODO Auto-generated constructor stub
}

public int getTeamId() {
	return teamId;
}

public void setTeamId(int teamId) {
	this.teamId = teamId;
}

public Set<Player> getPlayers() {
	return players;
}

public void setPlayers(Set<Player> players) {
	this.players = players;
}

public String getTeamName() {
	return teamName;
}

public void setTeamName(String teamName) {
	this.teamName = teamName;
}

public Team(Set<Player> players, String teamName) {
	super();
	this.players = players;
	this.teamName = teamName;
}



}