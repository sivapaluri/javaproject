package com.player.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "CricketMatch")
public class Match {
	
	

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	private String venue;
	
	@OneToOne(cascade = CascadeType.ALL)
	private Team team;


	public Match() {
		// TODO Auto-generated constructor stub
	}
	public String getVenue() {
		return venue;
	}

	public void setVenue(String venue) {
		this.venue = venue;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Team getTeam() {
		return team;
	}

	public Match(Team team , String venue) {
		super();
		this.venue = venue;
		this.team = team;
	}

	public void setTeam(Team team) {
		this.team = team;
	}

	

}
