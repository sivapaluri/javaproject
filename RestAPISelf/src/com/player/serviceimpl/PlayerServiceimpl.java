package com.player.serviceimpl;

import java.util.List;

import com.player.DAO.PlayerDAO;
import com.player.DaoImpl.PlayerDaoImpl;
import com.player.model.Player;
import com.player.model.Team;
import com.player.service.PlayerService;

public class PlayerServiceimpl implements PlayerService {

	private PlayerDAO dao = new PlayerDaoImpl();

	@Override
	public Player createPlayer(Player player) {
		// TODO Auto-generated method stub
		return dao.createPlayer(player);
	}

	@Override
	public Player updatePlayer(Player player) {
		// TODO Auto-generated method stub
		return dao.updatePlayer(player);
	}

	@Override
	public Player getPlayerById(int id) {
		// TODO Auto-generated method stub
		return dao.getPlayerById(id);
	}

	@Override
	public void removePlayer(int id) {
		// TODO Auto-generated method stub
		dao.removePlayer(id);;
	}

	@Override
	public List<Player> getallPlayers() {
		// TODO Auto-generated method stub
		return dao.getallPlayers();
	}


}
