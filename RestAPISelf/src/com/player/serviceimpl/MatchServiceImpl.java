package com.player.serviceimpl;

import com.player.DAO.MatchDAO;
import com.player.DaoImpl.MatchDAOImpl;
import com.player.model.Match;
import com.player.service.MatchService;

public class MatchServiceImpl implements MatchService {
	
	MatchDAO mDAO = new MatchDAOImpl();

	@Override
	public Match createMatch(Match match) {
		// TODO Auto-generated method stub
		return mDAO.createMatch(match);
	}

}
