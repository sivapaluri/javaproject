package com.player.DAO;

import com.player.model.Match;

public interface MatchDAO {

	public Match createMatch(Match match);
}
