package com.player.DAO;

import com.player.model.Team;

public interface TeamDAO {
	
	public Team createTeam(Team team);

}
