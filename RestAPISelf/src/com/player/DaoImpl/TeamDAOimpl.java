package com.player.DaoImpl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import com.player.DAO.TeamDAO;
import com.player.model.Team;

public class TeamDAOimpl implements TeamDAO{
	
	Configuration configuration = new Configuration().configure(); // pulling the configruation details by
	// identifying the .cfg file

	StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder()
			.applySettings(configuration.getProperties()); // getting properties in key value pairs

	SessionFactory factory = configuration.buildSessionFactory(builder.build()); // generating session factory using
	// builder

	@Override
	public Team createTeam(Team team) {
		// TODO Auto-generated method stub
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		session.save(team);
		transaction.commit();
		session.close();
		return team;
	}

}
