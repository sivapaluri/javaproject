package com.player.DaoImpl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import com.player.DAO.PlayerDAO;
import com.player.model.Player;
import com.player.model.Team;

public class PlayerDaoImpl implements PlayerDAO {
	
	Configuration configuration = new Configuration().configure(); // pulling the configruation details by
	// identifying the .cfg file

	StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder()
			.applySettings(configuration.getProperties()); // getting properties in key value pairs

	SessionFactory factory = configuration.buildSessionFactory(builder.build()); // generating session factory using
	// builder

	@Override
	public Player createPlayer(Player player) {
		// TODO Auto-generated method stub
		
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		session.save(player);
		transaction.commit();
		session.close();
		return player;
	}

	@Override
	public Player updatePlayer(Player player) {
		// TODO Auto-generated method stub
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		session.update(player);
		transaction.commit();
		session.close();
		return player;
	}

	@Override
	public Player getPlayerById(int id) {
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		Player player = (Player) session.get(Player.class,id);
		transaction.commit();
		session.close();
		return player;
	}

	@Override
	public void removePlayer(int playerId) {
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		Player p = new Player();
		p.setPlayerId(playerId);
		session.delete(p);
		transaction.commit();
		session.close();
	}

	@Override
	public List<Player> getallPlayers() {
		// TODO Auto-generated method stub
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		List<Player> playerList=session.createQuery("from com.player.model.Player").list();
		return playerList;
	}

	

}
