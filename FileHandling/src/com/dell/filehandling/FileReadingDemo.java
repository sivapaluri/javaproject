package com.dell.filehandling;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class FileReadingDemo {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		
		//variable declaration
		int ch;
		
		//check if file exists or not
		FileReader fr =null;
		try {
			fr= new FileReader("a.txt");
		}
		catch(FileNotFoundException fe)
		{
			System.out.println("file not found");
		}
		//read from file reader till the end of file
		
		while((ch=fr.read())!=-1)
			System.out.println(ch);
		
		//close the file
		fr.close();

	}

}
