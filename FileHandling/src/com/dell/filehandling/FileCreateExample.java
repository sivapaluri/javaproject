package com.dell.filehandling;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class FileCreateExample {

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub

		File file = new File("C:\\Users\\palurs1\\Downloads\\Filecreatedemo.txt");
		try {
			boolean createfile = file.createNewFile();
			if (createfile) {
				System.out.println("New file is created");
			} else {
				System.out.println("File already exists");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		//giving full path to create a file
		File file1 = new File("C:\\Users\\palurs1\\Downloads\\Filecreatedemo2.txt");
		try {
			boolean createfile = file.createNewFile();
			if(createfile) {
				System.out.println("file2created");
			}else {
				System.out.println("File already exists");
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
		//defining a parent location
		File parent = new File("C:\\Users\\palurs1\\");
		
		//using the parent location
		File file3 = new File(parent, "Downloads\\Filecreatedemo3.txt");
		
		try {
			boolean createfile = file.createNewFile();
			if(createfile) {
				System.out.println("file3created");
			}else {
				System.out.println("File already exists");
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		System.out.println(file.getAbsolutePath());
		
		//reading the data from the file
		FileReader fr = new FileReader("C:\\Users\\palurs1\\Downloads\\Filecreatedemo.txt");
		int i;
		while((i=fr.read())!=-1)
			System.out.print((char)i);
		fr.close();

	}

}