package com.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.demo.model.Message;
import com.demo.service.MessageService;

@RestController
@CrossOrigin
public class MessageController {
	
	@Autowired //Field Injection
	private MessageService service;
	
	@PostMapping("/message")
	public Message createMessage(@RequestBody Message message) {
		
		return service.createMessage(message);
	}
		

}
