package com.demo.service;

import java.util.List;

import com.demo.model.Message;

public interface MessageService {
	
	public Message createMessage(Message message);
	public Message getMessageById(int id);
	public List<Message> getAllMessages();
	public Message updateMessage(Message message);
	public void removeMessage(int id);

}
